package com.supinfo.remyo.cage;

import android.graphics.Matrix;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.Button;
import android.widget.ImageView;
import android.view.View;
import android.view.View.OnClickListener;

public class MainActivity extends AppCompatActivity {

    Button button;
    ImageView image;

    Matrix matrix = new Matrix();
    Float scale = 1f;
    ScaleGestureDetector SGD;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        play();

        //new
        image = (ImageView) findViewById(R.id.imageView2);
        SGD = new ScaleGestureDetector(this,new ScaleListener());
    }
    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener
    {
       @Override
        public boolean onScale(ScaleGestureDetector detector)
       {
           scale = scale*detector.getScaleFactor();
           scale = Math.max(0.1f, Math.min(scale,5f));
           matrix.setScale(scale,scale);
           image.setImageMatrix(matrix);
           return true;

       }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        SGD.onTouchEvent(event);
        return true;
    }

    public void play()
    {
        image = (ImageView) findViewById(R.id.imageView2);

        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View arg0) {
                image.setImageResource(R.drawable.whereiscage1);
            }

        });

    }
    }


